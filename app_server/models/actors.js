import mongoose from "mongoose";
import idPlugin from "mongoose-id";

const actorSchema = new mongoose.Schema({
	name: {type: String, default: ""}
});

actorSchema.plugin(idPlugin);
mongoose.model("Actor", actorSchema);
