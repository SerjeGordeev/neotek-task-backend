import mongoose from "mongoose";
import idPlugin from "mongoose-id";

const countrySchema = new mongoose.Schema({
	name: {type: String, default: ""}
});

countrySchema.plugin(idPlugin);
mongoose.model("Country", countrySchema);
