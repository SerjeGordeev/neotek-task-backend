import mongoose from "mongoose";
import idPlugin from "mongoose-id";

const directorsSchema = new mongoose.Schema({
	name: {type: String, default: ""}
});

directorsSchema.plugin(idPlugin);
mongoose.model("Directors", directorsSchema);
