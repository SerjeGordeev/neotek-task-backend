import mongoose from "mongoose";
import idPlugin from "mongoose-id";

const genreSchema = new mongoose.Schema({
	name: {type: String, default: ""}
});

genreSchema.plugin(idPlugin);
mongoose.model("Genre", genreSchema);
