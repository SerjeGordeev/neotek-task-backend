import express from "express";
import "./models/db";
import "./controllers/utils/common";

import rotesApi from "./routes";
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/",express.static("app_client"));
app.use("/api", rotesApi);
app.use("/attachments",express.static("attachments"));

export  default app;