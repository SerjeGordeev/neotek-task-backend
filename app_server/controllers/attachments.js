import mongoose from "mongoose";
import {createApi} from "./utils/createApi";
import multiparty from "multiparty";
import fs from "fs";

const Attachment = mongoose.model("Attachment");

let attachmentsMethods = createApi(Attachment, "Файл");

attachmentsMethods.create = function(req, res){
	var attachment = new Attachment();
	let form = new multiparty.Form();
	form.parse(req);
	form.on('file', function(name,file){
		attachment.url = `attachments/${file.originalFilename}`;
		attachment.name = file.originalFilename;

		fs.readFile(file.path, function (err, data) {
			if (err) throw err;
			// Write the file
			fs.writeFileSync(attachment.url, data);

			// Delete the file
			fs.unlink(file.path, function (err) {
				if (err) throw err;
			});
		});

		return attachment.save().then(attachment=>{
			res.json(attachment);
		});
	})
};

module.exports = attachmentsMethods;