import mongoose from "mongoose";
import {createApi} from "./utils/createApi";

const Country = mongoose.model("Country");

let countriesMethods = createApi(Country, "Страна");

module.exports = countriesMethods;