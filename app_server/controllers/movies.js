import mongoose from "mongoose";
import {createApi} from "./utils/createApi";

const Movie = mongoose.model("Movie");

let moviesMethods = createApi(Movie, "Фильм");

module.exports = moviesMethods;