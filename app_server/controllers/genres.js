import mongoose from "mongoose";
import {createApi} from "./utils/createApi";

const Genre = mongoose.model("Genre");

let genresMethods = createApi(Genre, "Жанр");

module.exports = genresMethods;