import mongoose from "mongoose";
import {createApi} from "./utils/createApi";

const Directors = mongoose.model("Directors");

let directorsMethods = createApi(Directors, "Режиссер");

module.exports = directorsMethods;