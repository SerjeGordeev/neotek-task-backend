import mongoose from "mongoose";
import {createApi} from "./utils/createApi";

const Actor = mongoose.model("Actor");

let actorsMethods = createApi(Actor, "Актер");

module.exports = actorsMethods;