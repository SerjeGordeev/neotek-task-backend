var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var NODE_ENV = process.env.NODE_ENV;
var nodeModules = {};

fs.readdirSync('node_modules')
    .filter(function(x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function(mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });

module.exports = {
    entry: './app_server/app.js',
    target: 'node',
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'server.build.js'
    },
    module: {
        loaders:[
            {test: /\.js$/, loader: "babel-loader"}
        ]
    },
    externals: nodeModules,
    plugins: [
        new webpack.IgnorePlugin(/\.(css|less)$/),
        new webpack.BannerPlugin('require("source-map-support").install();',
            { raw: true, entryOnly: false })
    ]
};

if(NODE_ENV == "production"){
    module.exports.plugins.push(
        new UglifyJSPlugin({
            compress:{
                unsafe: false
            },
            mangle: false
        })
    )
}